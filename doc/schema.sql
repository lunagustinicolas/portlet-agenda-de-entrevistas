--
-- Esquema con tabla de prueba para el ejemplo con Spring Data JPA
--
DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario(
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    msisdn VARCHAR(15) NOT NULL,
    codigo_verificador VARCHAR(150)
);

CREATE UNIQUE INDEX msisdn_unique
ON usuario(msisdn);

insert into usuario (msisdn, codigo_verificador) values
('1145457800', '1234'),
('1145006987', null),
('1155565150', '3244');