<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<portlet:resourceURL id="saludar" var="urlSaludar" />
<portlet:resourceURL id="restTemplate" var="urlRestTemplate" />
<portlet:resourceURL id="usuarioSpringData" var="urlUsuarioSpringData" />
<portlet:resourceURL id="paisesSpringData" var="urlPaisesSpringData" />

<h1>Portlet prueba con Spring para Liferay 7</h1>
<ul>
    <li><a href="<c:out value="${urlSaludar}" />">Servicio Saludar</a></li>
    <li><a href="<c:out value="${urlRestTemplate}" />">Servicio con RestTemplate</a></li>
    <li><a href="<c:out value="${urlUsuarioSpringData}" />">Servicio de Usuario con Spring Data JPA</a></li>
    <li><a href="<c:out value="${urlPaisesSpringData}" />">Servicio de Paises con Spring Data JPA</a></li>
</ul>

<h1>Paises</h1>
<ul>
    <c:forEach items="${paises}" var="pais">
        <li><c:out value="${pais.nombre}"/>
            <ul>
                <c:forEach items="${pais.usuarios}" var="usuario">
                    <li><c:out value="${usuario.msisdn}"/></li>
                </c:forEach>
            </ul>
        </li>
    </c:forEach>
</ul>