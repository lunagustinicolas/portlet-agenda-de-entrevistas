package com.somospnt.portlet.mvc.view.controller;

import com.liferay.portletmvc4spring.bind.annotation.RenderMapping;
import com.liferay.portletmvc4spring.bind.annotation.ResourceMapping;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import javax.portlet.ResourceResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.somospnt.portlet.mvc.business.service.EjemploService;
import com.somospnt.portlet.mvc.domain.Pais;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

@Controller
@RequestMapping("VIEW")
public class PortletViewController {

    private static Log logger = LogFactoryUtil.getLog(PortletViewController.class);
        
    private final EjemploService ejemploService;

    public PortletViewController(EjemploService ejemploService) {
        this.ejemploService = ejemploService;
    }

    @RenderMapping
    @Transactional
    public String vistaPrincipal(Model model) {
        logger.info("Buscando paises...");
        List<Pais> paises = ejemploService.buscarTodosPaises();
        //Se recorre la lista lazy para hidratarla con los objetos ya que no se encontro la forma de asociar 
        //a la vista en openInViewFilter para mantener abierta la sesion de Spring data.
        //Se aconseja no usar el @OneToMany.
        //En caso de que se tenga la lista con @OneToMany, se aconseja no hacer listas Lazy, en cambio utilizar el @FetchMode y/o @BatchSize para hacer mas performante la query.
        //En caso que la lista si sea Lazy se debe anotar al metodo del controller con @Transactional y recorrer la lista ejecutando el metodo size() para que se hidrate.
        paises.forEach(pais -> pais.getUsuarios().size());
        logger.info(paises.size() + " paises encontrados.");
        model.addAttribute("paises", paises);
        return "view";
    }

    @ResourceMapping("saludar")
    public void saludar(ResourceResponse resourceResponse) throws IOException {
        escribirResponse(resourceResponse, "hola mundo");
    }

    @ResourceMapping("restTemplate")
    public void restTemplate(ResourceResponse resourceResponse) throws IOException {
        escribirResponse(resourceResponse, ejemploService.getDeRestTemplate());
    }

    @ResourceMapping("usuarioSpringData")
    public void usuarioSpringData(ResourceResponse resourceResponse) throws IOException {
        escribirResponse(resourceResponse, ejemploService.buscarTodosUsuarios().toString());
    }

    @ResourceMapping("paisesSpringData")
    public void paisesSpringData(ResourceResponse resourceResponse) throws IOException {
        escribirResponse(resourceResponse, ejemploService.buscarTodosPaises().toString());
    }

    private void escribirResponse(ResourceResponse resourceResponse, String texto) throws IOException {
        OutputStream outputStream = resourceResponse.getPortletOutputStream();
        outputStream.write(texto.getBytes(Charset.forName("UTF-8")));
        outputStream.flush();
    }

}
