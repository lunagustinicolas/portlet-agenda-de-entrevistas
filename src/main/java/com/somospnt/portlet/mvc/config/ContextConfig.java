package com.somospnt.portlet.mvc.config;

import com.liferay.portal.util.PropsValues;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;

@Configuration
public class ContextConfig {

    //To resolve ${} in @Value
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
        pspc.setLocation(new FileSystemResource(PropsValues.LIFERAY_HOME + "/properties/portlet-mvc.properties"));
        return pspc;
    }
}
