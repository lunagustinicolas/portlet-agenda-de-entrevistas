package com.somospnt.portlet.mvc.config;

import java.util.Properties;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Configura un datasource, el entityManager, transacciones y demás beans
 * necesarios para usar JPA y Spring Data JPA. Si no se usan estas tecnologías,
 * esta clase puede quitarse (junto con las dependencias en el pom.xml). Todos
 * los atributos de esta clase deben ser configurados con los valores adecuados.
 * Para Spring Data, es necesario también configurar el tag jpa:repositories en
 * el archivo WEB-INF/spring-context/portlet/portlet-mvc-portlet.xml
 */
@Configuration
@EnableTransactionManagement
public class JpaConfig {

    @Value("${jndi.url:#{null}}")
    private String jndiUrl;

    @Value("${spring.datasource.driver:#{null}}")
    private String jdbcDriver;
    @Value("${spring.datasource.url:#{null}}")
    private String jdbcUrl;
    @Value("${spring.datasource.username:#{null}}")
    private String jdbcUsername;
    @Value("${spring.datasource.password:#{null}}")
    private String jdbcPassword;

    @Value("${spring.jpa.properties.hibernate.dialect}")
    private String hibernateDialect;
    @Value("${pnt.jpa.packagesToScan}")
    private String jpaPackagesToScan;

    @Bean(name = "dataSource", destroyMethod = "")
    public DataSource dataSource() throws NamingException {
        DataSource dataSource;

        if (jndiUrl == null) {
            dataSource = configurarDiverManagerDataSource();
        } else {
            dataSource = configurarJndiDataSource();
        }

        return dataSource;
    }

    private DataSource configurarJndiDataSource() throws NamingException {
        DataSource dataSource;
        dataSource = (DataSource) new JndiTemplate().lookup(jndiUrl);
        return dataSource;
    }

    private DriverManagerDataSource configurarDiverManagerDataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(jdbcDriver);
        driverManagerDataSource.setUrl(jdbcUrl);
        driverManagerDataSource.setUsername(jdbcUsername);
        driverManagerDataSource.setPassword(jdbcPassword);
        return driverManagerDataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NamingException {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(new String[]{jpaPackagesToScan});

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());

        return em;
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "none");
        properties.setProperty("hibernate.dialect", hibernateDialect);
        properties.setProperty("hibernate.physical_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");

        return properties;
    }

    @Bean
    public PlatformTransactionManager transactionManager() throws NamingException {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

}
