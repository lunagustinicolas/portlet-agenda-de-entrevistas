package com.somospnt.portlet.mvc.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String msisdn;
    @Column(name = "codigo_verificador")
    private String codVerificador;
    @ManyToOne
    private Pais pais;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getCodigoVerificador() {
        return codVerificador;
    }

    public void setCodigoVerificador(String codigoVerificador) {
        this.codVerificador = codigoVerificador;
    }

    public String getCodVerificador() {
        return codVerificador;
    }

    public void setCodVerificador(String codVerificador) {
        this.codVerificador = codVerificador;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", msisdn=" + msisdn + ", codVerificador=" + codVerificador + '}';
    }

}
