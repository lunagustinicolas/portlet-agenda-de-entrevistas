package com.somospnt.portlet.mvc.business.service;

import com.somospnt.portlet.mvc.domain.Pais;
import com.somospnt.portlet.mvc.domain.User;
import com.somospnt.portlet.mvc.repository.PaisRepository;
import com.somospnt.portlet.mvc.repository.RestTemplateRepository;
import com.somospnt.portlet.mvc.repository.UsuarioRepository;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EjemploService {

    private final RestTemplateRepository restTemplateRepository;
    private final UsuarioRepository usuarioRepository;
    private final PaisRepository paisRepository;

    public EjemploService(RestTemplateRepository restTemplateRepository, UsuarioRepository usuarioRepository, PaisRepository paisRepository) {
        this.restTemplateRepository = restTemplateRepository;
        this.usuarioRepository = usuarioRepository;
        this.paisRepository = paisRepository;
    }


    public String getDeRestTemplate() {
        return restTemplateRepository.getDeRestTemplate();
    }

    public List<User> buscarTodosUsuarios() {
        return usuarioRepository.findAll();
    }
    
    public List<Pais> buscarTodosPaises() {
        return paisRepository.findAll();
    }

}
