package com.somospnt.portlet.mvc.repository;

import com.somospnt.portlet.mvc.domain.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<User, Long>{

    Optional<User> findByMsisdn(String msisdn);

}
