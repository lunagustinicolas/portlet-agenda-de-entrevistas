package com.somospnt.portlet.mvc.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class RestTemplateRepository {

    @Autowired
    private RestTemplate restTemplate;
    @Value("${pnt.rest.url}")
    private String restUrl;

    public String getDeRestTemplate() {
        return restTemplate.getForEntity(restUrl, String.class).getBody();
    }

}
