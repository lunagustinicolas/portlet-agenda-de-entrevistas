package com.somospnt.portlet.mvc.repository;

import com.somospnt.portlet.mvc.domain.Pais;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaisRepository extends JpaRepository<Pais, Long>{

}
