package com.somospnt.portlet.mvc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@Sql({"/schema.sql", "/data.sql"})
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        locations = {"file:src/main/webapp/WEB-INF/spring-context/portlet/portlet-mvc-portlet.xml"})
public class BaseTest {

    @Test
    public void contextLoad() {

    }

}
