package com.somospnt.portlet.mvc.business.service;

import com.somospnt.portlet.mvc.BaseTest;
import com.somospnt.portlet.mvc.domain.User;
import java.util.List;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class EjemploServiceTest extends BaseTest {

    @Autowired
    private EjemploService ejemploService;

    @Test
    public void testJpaRepository() {
        List<User> usuarios = ejemploService.buscarTodosUsuarios();
        assertTrue(!usuarios.isEmpty());
        for (User usuario : usuarios) {
            assertNotNull(usuario.getPais());
            assertNotNull(usuario.getPais().getUsuarios());
            assertFalse(usuario.getPais().getUsuarios().isEmpty());
        }
    }

    @Test
    public void testRestTemplate() {
        String body = ejemploService.getDeRestTemplate();
        assertFalse(body.isEmpty());
    }

}
