# Portlet con Spring Framework para Liferay 7
Este proyecto contiene un portlet b�sico para Liferay 7 que utiliza Spring
Framework, Spring MVC y Spring Data JPA (con Hibernate).

Este proyecto fue probado con Liferay 7 en el bundle de Tomcat, y con Liferay 7
instalado sobre WebLogic 12c.

Este proyecto NO utiliza Spring Boot para la configuraci�n de Spring.

## Archivos importantes
### pom.xml
Contiene las librer�as b�sicas necesarias para el funcionamiento del proyecto.
Es muy importante respetar las versiones, porque sino ocurren conflictos con
Liferay.

En particular, el conjunto de librer�as necesarias para Spring Data JPA puede
omitirse si no se utiliza este framework. De omitirse, es tambi�n necesario
quitar la referencia en el archivo WEB-INF/spring-context/portlet/portlet-mvc-portlet.xml
(quitar el schema de data y el tag jpa:repositories)

### WEB-INF/web.xml
Contiene la configuraci�n necesaria para levantar el contexto de Spring.
No deber�a editarse, ni agregar o quitar nada. Deber�a funcionar as� como est�.

### WEB-INF/portlet-xml
Contiene informaci�n del portlet.
S�lo deber�a editarse para configurar el nombre del portlet.

### WEB-INF/spring-context/portlet-application-context.xml
Contiene la configuraci�n de Spring general para atender las vistas.
No deber�a editarse, ni agregar o quitar nada. Deber�a funcionar as� como est�.

### WEB-INF/spring-context/portlet/portlet-application-context.xml
Contiene el contexto de Spring para los portlets.
Es necesario configurar correctamente el paquete para el component-scan
(apuntando al paquete raiz donde se encuentran los beans de Spring) y el
paquete para los repositorios de Spring Data JPA.

### Clase com.somospnt.portlet.mvc.config.JpaConfig
Configuraci�n para Spirng Data JPA. Lee la configuraci�n de una archivo
application.properties para configurar el DataSource y otras caracter�sticas.

### application.properties
Contiene informaci�n del DataSource. Usa las mismas keys que Spring Boot, pero
el proyecto NO utiliza Spring Boot.
Adem�s, es necesario configurar la property pnt.jpa.packagesToScan que debe
apuntar al paquete raiz donde se encuentran las @Entity.

### properties/portlet-mvc-log4j-ext.xml
La configuraci�n de log del portlet. El archivo tiene que ubicarse en 
LIFERAY_HOME/osgi/log4j/nombreportlet-log4j-ext.xml
Es importante utilizar el mismo nombre del portlet. Aqu� puede configurarse
el archivo de log, la prioridad por default y otros temas. Es necesario 
configurar la ruta donde queda el archivo en la propiedad FileNamePattern del
appender FILE.

### Configurar JNDI tomcat local
En *<tomcat_home>/conf/context.xml* agregar el siguiente resource:

```	
<Resource name="jdbc/LiferayPool"
			  auth="Container"
			  type="javax.sql.DataSource"
			  driverClassName="org.mariadb.jdbc.Driver"
			  url="jdbc:mariadb://localhost:3310/liferay"
			  username="liferay"
			  password="liferay"
			  maxTotal="20"
			  maxIdle="10"
			  maxWaitMillis="-1"/>		  
```